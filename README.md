# Coffee / Lunch Meet with Coworkers
## Description

This simple Vue.js application allows new hires to enter their name into the system, and matches them with a coworker for coffee or a group of coworkers for lunch.

Coffee: The applications pairs you with someone you haven't had coffee with before; otherwise it suggests lunch.
Lunch: The application pairs you with a random group to go eat lunch with and prioritizes people you haven't had lunch with before.

## Usage
To get started it is very simple. Simply open the file in your favorite browser and follow the instructions:
`lunch_coffee_with_coworkers.html`

## Implementation
The Vue.js data object the following keys:

 - `name`
 - `coworkers`
 - `relativeLunchFrequencyTable`

The `name` key is of type string and stores the name the new hire enters temporarily.

The `coworker` key stores an array of objects, each object being each coworker. The schema for that object is:
        
    {
        id: userId,
        name: this.name,
        coffeeMates: [],
        lunchMates: [[]]
    }

The `id` of each user is their index in the coworkers array. The `name` stores the name of each user. The `coffeeMates` array contains all the `id`s of other coworkers this coworkers has grabbed lunch with. Finally, the `lunchMates` array is the old impletementation of pairing people for lunch. Its usage is described in the `greedyGrabLunchOld` method described below.

The `relativeLunchFrequencyTable` is a table of the total number of lunch dates people have gone on with each other and is used for the `groupFocusedLunchGroup` algo, `greedyLunchGroup` algo and `weightedLunchGroup` algo:


    _______| Dopey | Sleepy | Freddy | Ludo
    Dopey  |       |   #    |    #   |   #
    Sleepy |  #    |        |    #   |   #
    Freddy |  #    |   #    |        |   #
    Ludo   |  #    |   #    |    #   |    


 
For our purposes we only need one triangle of the table (due to symmetry) so we will use the bottom triangle. 

In the diagonal instead of leaving it empty we can utilize it by putting the # of total lunches the user has gone on in it.

### Adding a new hire implementation
The purpose of the `addNewHire` method is to add new hires in the system.

- A new hire (object) is pushed into the coworkers array
- Every hire (object) contains:
    - the user's name, 
    - a unique userId (which is their index in the coworkers array), 
    - an array to save the users coffeeMates (initially empty), 
    - an array to store lunchMates (a 2D array) 
- The lunchMates array includes all of the coworkers `id`s in an array in the 0th element of the array (an array in an array a.k.a a 2D array)
- At the index of the user's new id in the `relativeLunchFrequencyTable` an array filled with all zeros is push. 
    - The length of the pushed array is the number of coworkers - 2 
    - Its length is so because the new hire should not be included in the table and the 0th user (a.k.a the first hire) has an empty array

To better imagine the `relativeLunchFrequencyTable` array it looks like so for 4 users in the system (including the new hire):

    [
        [],
        [0],
        [0,0],
        [0,0,0]
    ]

NOTICE: This array is symmetric so `relativeLunchFrequencyTable[0][1]` is in fact equivalent to `relativeLunchFrequencyTable[1][0]`.Furthermore, the only thing stored in a repeated index such as `relativeLunchFrequencyTable[0][0]` is the total number of lunch meets the user with `id` 0 has been on.

### Coffee Meets Implementation
The `grabCoffee` method pairs the user with someone they haven't grabbed coffee with before. The method evaluates the users `coffeeMates` array and sees if it contains any index (i.e. user `id`) which is not in the coworkers array. It is fed the user requesting's `id` and `name` from the frontend thanks to Vue.js. The method functions as follows:

- Step 1: Verify they haven't had coffee with everyone (i.e. the length of the coworkers array - 1)
- Step 2: Loop through the coworkers array (excluding this users index)
- Step 3: At each index verify that the user's coffeeMates array does not include the coworkers unique id
- Step 4: Inform the user of who they've been paired with 
- Step 5: Push the coworkers id to the users coffeeMates array and vice-versa

### Lunch Meets Implementation
There are 4 methods I developed to group people for lunch meets. The methods have the following names: `greedyGrabLunchOld`, `greedyLunchGroup`, `groupFocusedLunchGroup` and `weightedLunchGroup` methods. The first method in this list uses the `lunchMates` array and the rest use the `relativeLunchFrequencyTable` array. 

In my humble opinion the use of the `relativeLunchFrequencyTable` array is much cleaner and more efficient than the use of the `lunchMates` array. Furthermore, the `groupFocusedLunchGroup`, again in my humble opinion, is the best and most morally sound way of selecting a group, democratizing the process.

#### The `greedyGrabLunchOld` Method Explained:
The purpose of this method is to group people for a lunch meet including the user and prioritize people the user hasn't had lunch with before

__NOTICE__: This is a greedy algorithm because it only cares about the people __YOU'VE__ (you being the user) been to lunch with the least and doesnt take others in the group into consideration. Also notice this is labeled the __OLD__ greedy method because it uses the lunchMates array is designed to be greedy. 

Before we begin explaining the method it is important to understand that `lunchMates` is a 2D array with the index of each row equaling the number of times you've been to lunch with people in the columns of that row. Here's a visualization to help us out, imagine this as our `lunchMates` array:

    [
        [1, 4, 6],
        [0, 2, 5],
        [7]
    ]
    
Firstly notice that the number 3 is missing from all the arrays because in this example that is the users/my index. In row 0 we have the following array `[1,4,6]` these are the coworkers I've been to lunch with 0 times, in row 1 the array `[0, 2, 5]` includes the `id`s of all the coworkers I've been to lunch with once and so on and so forth.

Now that you can visualize the `lunchMates` array, the method functions as follows:

- Step 1: We retrieve the lunchMates array specific to this user
- Step 2: We initialize the current lunch mates (new lunch group) to an empty array - to be filled with other group members
- Step 3: We loop over length of the lunchMates array (which is equal to the number of lunches the user has been on)
- Step 4: While there are people in the least frequently 'lunched' with group (i.e. lunchMates[0] then lunchMate[1], etc) add to the lunch group 
- Step 5: Verify that the number of coworkers in the group is between 3 and 5 AND that no one already in our group is being added again
- Step 6: Move the coworker from the current lunchMate row to the next row (figuratively in the 2d array that is lunchMates)
- Step 7: Move the user/me from the current lunchMate row to the next row (figuratively in the 2d array that is lunchMates) of the coworkers 
- Step 10: Inform the user of all the group members he's going with

#### The `greedyLunchGroup` Method Explained:
The purpose of this method is to create a lunch group that only cares to group you with the people __YOU'VE__ been with the least hence the name `greedyLunchGroup` algo because we only care about introducing you to new people and no one else in the group.

The algorithm is simple. It looks at your row in the `relativeLunchFrequencyTable` and finds the first user with the lowest number of mutual lunches, then the second and so on and so forth until you reach the maximum group size which is between 3 and 5. Then it informs you of the group and increments the number of mutuals visits for everyone in the table.

#### The `groupFocusedLunchGroup` Method Explained:
The purpose of this algorithm is to group people for lunch; however, this algoritm takes a unique approach (literally) by creating a group which is less traveled. Meaning this algorithm creates a group composed of members who have eaten with each other in total the least.
This method is able to do this by using the `relativeLunchFrequencyTable` and by calculating the `groupTotal`. This approach __democratizes__ the lunch groups and enables the most unique group to go to lunch with each other.

The `groupTotal` is an array the length of the # of coworkers. At first it contains all the lunch trips the user/me has had with everyone (the row in `relativeLunchFrequencyTable`). As we add users to the group their rows get added to the `groupTotal` so as we search for the next person we find the person which least frequently went to lunch with the remainder of the group. This is a group focused lunch group which prioritizes the diversity of the group with people who've least frequently went to lunch with each other.

As an example imagine the following: 

1. user D would like to go to lunch, 
2. His array/row (`groupTotal`) in the `relativeLunchFrequencyTable` is `[2,1,3,null,0,1]`
3. This array means he's been to lunch with coworkers:
    - A - 2 times
    - B - 1 time
    - C - 3 times
    - D - null because this is myself
    - E - 0 times
    - F - 1 time
4. Therefore D chooses to add E to the lunch group
5. user E has a row in the `relativeLunchFrequencyTable` equal to `[1,5,2,0,null,1]`
6. So we will add each corresponding element in user E's row to user D's row to create a DE row (a.k.a a new `groupTotal` array)
7. The new `groupTotal` array is `[3,6,5,null,null,2]`
8. Once again we by evaluating the new `groupTotal` we find that the most sensible person to add to the group to prioritize people you haven't had lunch is F
9. So we add F's row to the DE row (`groupTotal` array) to form a new `groupTotal` array DEF
10. We repeat this until we get a group size between 3 and 5

Notice the beauty of this algorithm; if E had it their way they could've chosen A since it was also equivalent to 1 but that would've been terrible for D because A is 2 for D. If D had it their way they could've chosen B since it was also equivalent to 1 but again that would've been terrible for E since B is 5 for E. Therefore, F was the most sensible option for both and it was beneficial for F to join this group; fill two needs with one deed or as they used to say on the playground a twofer.

#### The `weightedLunchGroup` Method Explained:
The `weightedLunchGroup` method is almost exactly the same as the `groupFocusedLunchGroup` except you can assign weights to people and their rows will be multiplied by that weight. The higher the weight the less important pairing them with someone mutually beneficial to them is.

This works because the `groupTotal` array functions by finding the minimum as it goes along. A new group memeber that has a row with a very high value will not have his/her opinions considered as much since they will lead to very large sums in the updated `groupTotal` array.

This is beneficial if you wanted to pair people together more frequently than others like the backend team with the frontend team or the execs with the managers, etc.

In the code example provided the method simply values peoples interests less and less as they get joined to the group. So the first add-on to the group (second person in group) will have their array multiplied by two, the following by three and so on and so forth.

## Author
Abraham Darwish - Oct. 2018


