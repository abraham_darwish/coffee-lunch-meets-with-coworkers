new Vue({
  el: "#app",
  data: {
    name: "",
    coworkers: [],
    relativeLunchFrequencyTable: []
    /*
     * The relativeLunchFrequencyTable is a table of all the lunch dates people have gone on with each other and are is used for the group-focused lunch algo, greedyLunchGroup algo and weightedLunchGroup algo
     * _______| Dopey | Sleepy | Freddy | Ludo
     * Dopey  |       |   #    |    #   |   #
     * Sleepy |  #    |        |    #   |   #
     * Freddy |  #    |   #    |        |   #
     * Ludo   |  #    |   #    |    #   |    
     * 
     * For our purposes we only need one triangle of the table so we are use the bottom triangle. 
     * In the diagonal instead of leaving it empty we can utilize it by putting the # of total lunches the user has gone on in it.
     */
  },
  methods: {
    addNewHire() {
      /*
       * The purpose of this method is to add new hires in the system.
       * A new hire (object) is pushed into the coworkers array
       * Every hire (object) contains:
       * the user's name, 
       * a unique userId, 
       * an array to save the users coffeeMates (initially empty), 
       * an array to store lunchMates (a 2D array) 
       * and adds the new hire intially as an array of all 0's in the relativeLunchFrequencyTable
       * */
      // Implement the addNewHire method only if the user inputs a value in the input field
      if (this.name.length > 0) {
        // We initialize the array of all the existing hires id's to empty
        let allHiresIds = [];

        // We generate a unique userId for the new hire which is their index in the coworkers array
        let userId = this.coworkers.length;

        /** We iterate through the array of the existing coworkers
         * We add each coworkers id to the allHiresIds array
         * We add the new hire's userId to each coworkers lunchMates array at index 0 (i.e. went to lunch together 0 times)
         */
        this.coworkers.forEach(person => {
          // For each coworker already in our system we take their id and put them in the allHireIds array because we need to initialize the lunch mates array at index 0 (a.k.a 0 lunch visits with coworkers)
          allHiresIds.push(person.id);
          // We also need to add this new hire's id to every coworkers lunchMates array at index 0 because they have not yet gone out to lunch with the new hire
          person.lunchMates[0].push(userId);
        });

        // Now we need to add the new hire to the coworkers array
        this.coworkers.push({
          id: userId, // Set the new hire's id to the userId we generated above
          name: this.name, // Set the name of the new hire
          coffeeMates: [], // coffeMates is an array of all the people you've had coffee with
          lunchMates: [allHiresIds] // lunchMates is a 2d array index 0 contains an array of all the people you've never had lunch with, index 1 contains an array of all the people you've had lunch with once and so on and so forth
        });

        // Reset this.name in the Vue model
        this.name = "";

        // Add the new hire to the relativeLunchFrequencyTable
        this.relativeLunchFrequencyTable.push([]); // push the new user as an array into the table

        if (this.relativeLunchFrequencyTable.length > 0) {
          // Add at the nth element (# of coworkers) a value of 0 which is the number of total times the person has been to lunch
          this.relativeLunchFrequencyTable[
            this.relativeLunchFrequencyTable.length - 1
          ][this.relativeLunchFrequencyTable.length - 2] = 0;

          // Fill the entire array with 0 becuase relative to everyone the new hire has not gone out to lunch with anyone
          this.relativeLunchFrequencyTable[
            this.relativeLunchFrequencyTable.length - 1
          ].fill(0, 0, this.relativeLunchFrequencyTable.length - 2);
        }
      }
    },
    grabCoffee(userId, personName) {
      /*
       * The purpose of this method is to pair the user with someone they haven't grabbed coffee with before.
       * Step 1: Verify they haven't had coffee with everyone (i.e. the length of the coworkers array - 1)
       * Step 2: Loop through the coworkers array (excluding this users index)
       * Step 3: Retrieve the coworkers unique id
       * Step 4: Verify that the user's coffeeMates array does not include the coworkers unique id
       * Step 5: Inform the user of who they've been paired with 
       * Step 6: Push the coworkers id to the users coffeeMates array and vice-versa
       */

      let myCoffeeMates = this.coworkers[userId]["coffeeMates"]; // array of my coffeeMates

      // Step 2:
      // If I have grabbed coffee with more than or equal to the number of coworkers in the office (-1 because I'm included in the coworkers array) then inform the user and choose someone at random from the array again

      if (myCoffeeMates.length >= this.coworkers.length - 1) {
        // It seems you've had coffee with everyone message
        document.getElementById("coffeeMateMessage").innerHTML =
          "You've already caffeinated with everyone! Try lunch instead.";
      } else {
        // Step 3:
        // If I have not gone out with all of my coworkers
        // Go through a loop of all the coworkers in the office to select a new coffee partner
        for (let index = 0; index < this.coworkers.length; index++) {
          // You can't grab coffee with yourself
          if (index !== userId) {
            // Step 4:
            let coworker = this.coworkers[index];
            let coworkersId = coworker.id;

            // Step 5:
            // If:
            // Condition 1: my coworkers id is not save in my coffeeMates array (i.e. we have not been coffee mates in the past)
            // Condition 2: AND my coworkers id is not my id (we are double checking here because this should have been verified in the previous if statement)
            // True: match the two coworkers together
            if (
              myCoffeeMates.indexOf(coworkersId) === -1 &&
              coworkersId !== userId
            ) {
              console.log("Coffee mate found!");
              // Step 6:
              // Show user their coffee time message along with who they've been paired with
              document.getElementById("coffeeMateMessage").innerHTML =
                "Hey " +
                personName +
                ", you've been paired with " +
                coworker.name +
                " for coffee time!";

              // Step 7:
              // Save this coworker's id in my coffeeMates array and my id in their coffeeMate array for future reference
              myCoffeeMates.push(coworkersId);
              this.coworkers[index].coffeeMates.push(userId);

              // Now that we've found a coffeemate pair break out of the for loop
              break;
            }
          }
        }
      }
    },
    greedyGrabLunchOld(userId, personName) {
      /*
       * The purpose of this method is to group people for lunch including the user and prioritize people you haven't had lunch with before
       * NOTICE: This is a greedy algorithm because it only cares about the people YOU'VE been to lunch with the least and doesnt take others in the group into consideration. 
       * This is the OLD greedy method because it only takes YOU into account for the coworker and uses the lunchMates array
       * Step 1: We retrieve the lunchMates (myPreviousLunchMates) array specific to this userId by getting the index of the user in the coworkers array
       * Step 2: We initialize the current lunch mates (new lunch group) to an empty array - to be filled with other group members
       * Step 3: We loop over length of the myPreviousLunchMates array which is equal to the number of lunches the user has been on
       * Step 4: Verify that the number of coworkers in the group is enough (between 3 and 5)
       * Step 5: While there are people in the least frequently 'lunched' with group (i.e. lunchMates[0] then lunchMate[1], etc) add to the lunch group 
       * Step 6: Verify that the number of coworkers in the group is between 3 and 5 AND that no one already in our group is being added again
       * Step 7: Retrieve each coworker in the least common lunch group at index 0 (the coworker being added is always in index 0 because they are always removed after/spliced)
       * Step 8: Move the coworker from the current lunchMate row to the next row (figuratively in the 2d array that is lunchMates)
       * Step 9: Move the user from the current lunchMate row to the next row (figuratively in the 2d array that is lunchMates) of the coworkers 
       * Step 10: Inform the user of all the group members he's going with
       */

      // Retrieve the user's lunchMates (this 2d array includes all of the coworkers with each index representing the number of times the user has gone to lunch with them)
      let myPreviousLunchMates = this.coworkers[userId]["lunchMates"];
      // Initialize the current lunch mates for this visit to 0
      let myCurrentLunchMates = [];

      // Random group size between 3 and 5 - NOTICE: groupSize does not include myself
      let groupSize = Math.floor(Math.random() * 2) + 3;

      // The length of myPreviousLunchMates array is the number of total lunch trips I've made, so loop over all the lunch trips i've taken
      for (
        let lunchVisitTrip = 0; lunchVisitTrip < myPreviousLunchMates.length; lunchVisitTrip++
      ) {
        // If I have less than 3 people going to lunch with me I need to add more
        if (myCurrentLunchMates.length < groupSize) {
          // myPreviousLunchMates[lunchVisitTrip] array contains all the people I've gone to lunch with 'lunchVisitTrip' times
          while (myPreviousLunchMates[lunchVisitTrip].length > 0) {
            //If I have less than 3 people going to lunch with me I need to add more AND Only consider coworkers you are not considering already in the myCurrentLunchMates array
            if (
              myCurrentLunchMates.length < groupSize &&
              !myCurrentLunchMates.includes(
                myPreviousLunchMates[lunchVisitTrip][0]
              )
            ) {
              let coworkersId = myPreviousLunchMates[lunchVisitTrip][0];
              // Add the coworkers id to myCurrentLunchMates array for this trip
              myCurrentLunchMates.push(coworkersId);

              // Increment the number of lunch visits for the for both myself and the coworker I'm going out to lunch with by moving/shifting the coworker out of myPreviousLunchMates[lunchVisitTrip] to myPreviousLunchMates[lunchVisitTrip + 1]

              // If the array exists then we can push to it otherwise we must intialize it and then add to it
              if (myPreviousLunchMates[lunchVisitTrip + 1]) {
                myPreviousLunchMates[lunchVisitTrip + 1].push(coworkersId);
              } else {
                myPreviousLunchMates[lunchVisitTrip + 1] = [];
                myPreviousLunchMates[lunchVisitTrip + 1].push(coworkersId);
              }

              myPreviousLunchMates[lunchVisitTrip].splice(0, 1);
              // Save changes to the global coworkers array
              this.coworkers[userId]["lunchMates"] = myPreviousLunchMates;
              // Shift the lunch visits for the coworker as well

              let coworkersLunchMates = this.coworkers[coworkersId][
                "lunchMates"
              ];

              if (coworkersLunchMates[lunchVisitTrip + 1]) {
                coworkersLunchMates[lunchVisitTrip + 1].push(userId);
              } else {
                coworkersLunchMates[lunchVisitTrip + 1] = [];
                coworkersLunchMates[lunchVisitTrip + 1].push(userId);
              }

              // Remove the userId from its current location in the selected coworkers lunchMates array
              coworkersLunchMates[lunchVisitTrip].splice(
                coworkersLunchMates[lunchVisitTrip].indexOf(userId), // index of the userId in the this coworkers lunchMates
                1
              );
            } else {
              break;
            }
          }
        } else {
          break;
        }
      }

      let lunchMateMessage =
        "Hey " +
        personName +
        ", why don't you grab lunch with this awesome group of people: <br>";
      myCurrentLunchMates.forEach(userId => {
        lunchMateMessage += this.coworkers[userId]["name"] + "<br>";
      });

      document.getElementById("lunchMateMessage").innerHTML = lunchMateMessage;
    },
    greedyLunchGroup(userId, personName) {
      /** The purpose of this method is to create a lunch group that only cares to group you with the people you've been with the least
       * Hence it is the greedLunchGroup algo because we only care about introducing you to new people and no one else in the group.
       */

      // The lunch group contains only myself right now
      let lunchGroup = [userId];

      /**
       * The groupTotal array is an array the length of the # of coworkers
       * It contains all the lunch trips the user has had with everyone (the row in relativeLunchFrequencyTable)
       * We will slowly fill the lunchGroup by evaluting coworkers with the lowest score in the groupTotal array
       */

      // initialize lunch group totals
      let groupTotal = [];
      let numberOfCoworkers = this.relativeLunchFrequencyTable.length;
      // Fill lunch group with users row
      for (let i = 0; i < numberOfCoworkers; i++) {
        if (i !== userId) {
          if (this.relativeLunchFrequencyTable[userId][i] !== undefined) {
            groupTotal[i] = this.relativeLunchFrequencyTable[userId][i];
          } else {
            groupTotal[i] = this.relativeLunchFrequencyTable[i][userId];
          }
        } else {
          groupTotal[i] = null;
        }
      }

      // Random group size between 3 and 5 - NOTICE: groupSize DOES include myself
      let groupSize = Math.floor(Math.random() * 3) + 3;

      // If the group size is larger than the number of coworkers take everyone out to lunch by appending everyone to the lunchGroup array
      if (groupSize > numberOfCoworkers) {
        // reset lunch group because this.coworkers includes me
        lunchGroup = [];
        this.coworkers.forEach(person => {
          lunchGroup.push(person.id);
        });
      } else {
        while (lunchGroup.length < groupSize) {
          // Find the next best person to join the group (which is the index with the lowest score)
          lunchMateIndex = groupTotal.indexOf(
            // Find the coworker with lowest cumulative lunches with everyone (i.e min in groupTotal)
            Math.min(
              // filter out the nulls because Math.min considers them 0's
              ...groupTotal.filter(function (val) {
                return val !== null;
              })
            )
          );

          // Add the person found to our lunch group
          lunchGroup.push(lunchMateIndex);

          // Stop considering them from groupTotal array by setting those users to null
          groupTotal[lunchMateIndex] = null;
        }
      }

      // Print out message to lunch group
      let lunchMateMessage =
        "Hey " +
        personName +
        ", why don't you grab lunch with this awesome group of people: <br>";

      for (let i = 0; i < lunchGroup.length; i++) {
        if (lunchGroup[i] !== userId) {
          lunchMateMessage += this.coworkers[lunchGroup[i]]["name"] + "<br>";
        }
      }

      document.getElementById("lunchMateMessage").innerHTML = lunchMateMessage;

      // Tally every coworker that every other coworker went to lunch with
      while (lunchGroup.length > 0) {
        for (let i = 1; i < lunchGroup.length; i++) {
          if (
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] !==
            undefined
          ) {
            this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
              lunchGroup[i],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] + 1
            );
          } else {
            this.relativeLunchFrequencyTable[lunchGroup[i]].splice(
              lunchGroup[0],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[i]][lunchGroup[0]] + 1
            );
          }
        }
        // Increment the number of lunch meets you've been on by 1
        let numberOfLunchMeetsUserHasBeenOn = 1; // intitallty the users been on 0 + 1 (this time)
        // If theyve been on lunch meets before then their index in the relativeLunchFrequencyTable should contain the number of times theyve been on a lunch meet
        if (
          this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] !==
          undefined
        ) {
          numberOfLunchMeetsUserHasBeenOn =
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] + 1;
        }

        this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
          lunchGroup[0],
          1,
          numberOfLunchMeetsUserHasBeenOn // increment the number of lunch meets by 1
        );
        // Remove the user we just tallied from the lunchGroup array
        lunchGroup.shift();
      }

      // Message about how many times you've been out to lunch
      document.getElementById("lunchFrequencyMessage").innerHTML =
        "P.S. Did you know you've been out to lunch with coworkers " +
        this.relativeLunchFrequencyTable[userId][userId] +
        (this.relativeLunchFrequencyTable[userId][userId] > 1 // This conditional statement does not need to take zero into account because the only reason you're printing out this statement is because they are going to lunch.
          ?
          " times! (including this)" :
          " time! (including this)");
    },
    groupFocusedLunchGroup(userId, personName) {
      /**
       * The purpose of this algorithm is to group people for lunch
       * This algoritm takes a unique approach (literally) by creating a group which is less traveled
       * meaning this algorithm creates a group composed of members who have eaten with eachother in total the least
       * It does this using the relativeLunchFrequencyTable and by calculating the groupTotal
       * This approach democratizes the lunch groups and enables the most unique group to go to lunch with each other.
       */

      // The lunch group contains only myself right now
      let lunchGroup = [userId];

      let numberOfCoworkers = this.relativeLunchFrequencyTable.length;

      // Random group size between 3 and 5 - NOTICE: groupSize DOES include myself
      let groupSize = Math.floor(Math.random() * 3) + 3;

      // If the group size is larger than the number of coworkers take everyone out to lunch by appending everyone to the lunchGroup array
      if (groupSize > numberOfCoworkers) {
        // reset lunch group because this.coworkers includes me
        lunchGroup = [];
        this.coworkers.forEach(person => {
          lunchGroup.push(person.id);
        });
      } else {
        /**
         * The groupTotal array is an array the length of the # of coworkers
         * At first it contains all the lunch trips the user has had with everyone (the row in relativeLunchFrequencyTable)
         * As we add users to the group their rows get added to the groupTotal so as we search for the next person we find the person which least frequently went to lunch with the remainder of the group
         * This is a group focused lunch group which prioritizes the diversity of the group with people who've least frequently went to lunch with each other
         */
        // initialize lunch group totals
        let groupTotal = [];
        // Fill lunch group with my users row
        for (let i = 0; i < numberOfCoworkers; i++) {
          if (i !== userId) {
            if (this.relativeLunchFrequencyTable[userId][i] !== undefined) {
              groupTotal[i] = this.relativeLunchFrequencyTable[userId][i];
            } else {
              groupTotal[i] = this.relativeLunchFrequencyTable[i][userId];
            }
          } else {
            groupTotal[i] = null;
          }
        }

        while (lunchGroup.length < groupSize) {
          // Find the next best person to join the group (which is the index with the lowest score)
          lunchMateIndex = groupTotal.indexOf(
            // Find the coworker with lowest cumulative lunches with everyone (i.e min in groupTotal)
            Math.min(
              // filter out the nulls because Math.min considers them 0's
              ...groupTotal.filter(function (val) {
                return val !== null;
              })
            )
          );

          // Add the person found to our lunch group
          lunchGroup.push(lunchMateIndex);
          // Update the groupTotal array with the people in the lunch group
          for (let i = 0; i < numberOfCoworkers; i++) {
            if (i !== lunchMateIndex) {
              if (groupTotal[i] !== null) {
                if (
                  this.relativeLunchFrequencyTable[lunchMateIndex][i] !==
                  undefined
                ) {
                  groupTotal[i] += this.relativeLunchFrequencyTable[
                    lunchMateIndex
                  ][i];
                } else {
                  groupTotal[i] += this.relativeLunchFrequencyTable[i][
                    lunchMateIndex
                  ];
                }
              }
            } else {
              groupTotal[i] = null;
            }
          }
        }
      }

      // Print out message to lunch group
      let lunchMateMessage =
        "Hey " +
        personName +
        ", why don't you grab lunch with this awesome group of people: <br>";

      for (let i = 0; i < lunchGroup.length; i++) {
        if (lunchGroup[i] !== userId) {
          lunchMateMessage += this.coworkers[lunchGroup[i]]["name"] + "<br>";
        }
      }

      document.getElementById("lunchMateMessage").innerHTML = lunchMateMessage;

      // Tally every coworker that every other coworker went to lunch with
      while (lunchGroup.length > 0) {
        for (let i = 1; i < lunchGroup.length; i++) {
          if (
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] !==
            undefined
          ) {
            this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
              lunchGroup[i],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] + 1
            );
          } else {
            this.relativeLunchFrequencyTable[lunchGroup[i]].splice(
              lunchGroup[0],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[i]][lunchGroup[0]] + 1
            );
          }
        }
        // Increment the number of lunch meets you've been on by 1
        let numberOfLunchMeetsUserHasBeenOn = 1; // intitallty the users been on 0
        // If theyve been on lunch meets before then their index in the relativeLunchFrequencyTable should contain the number of times theyve been on a lunch meet
        if (
          this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] !==
          undefined
        ) {
          numberOfLunchMeetsUserHasBeenOn =
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] + 1;
        }

        this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
          lunchGroup[0],
          1,
          numberOfLunchMeetsUserHasBeenOn // increment the number of lunch meets by 1
        );
        // Remove the user we just tallied from the lunchGroup array
        lunchGroup.shift();
      }

      // Let the user know how many times they've been out to lunch
      document.getElementById("lunchFrequencyMessage").innerHTML =
        "P.S. Did you know you've been out to lunch with coworkers " +
        this.relativeLunchFrequencyTable[userId][userId] + // this is the number of times
        (this.relativeLunchFrequencyTable[userId][userId] > 1 // If you've been out less than 1 time the message should say 'time' instead of 'times'
          ?
          " times! (including this)" :
          " time! (including this)");
    },
    weightedLunchGroup(userId, personName) {
      /**
       * The purpose of this algorithm is to group people for lunch
       * This algoritm takes a measured approach by creating a group by weighing people who get selected first higher than people selected later in matching them thereby prioritizing people you haven't been to lunch with and a weighted group
       * It does this using the relativeLunchFrequencyTable and by calculating the groupTotal
       * This approach diversifies the lunch groups but values people with weights.
       */

      // The lunch group contains only myself right now
      let lunchGroup = [userId];

      let numberOfCoworkers = this.relativeLunchFrequencyTable.length;

      // Random group size between 3 and 5 - NOTICE: groupSize DOES include myself
      let groupSize = Math.floor(Math.random() * 3) + 3;

      // If the group size is larger than the number of coworkers take everyone out to lunch by appending everyone to the lunchGroup array
      if (groupSize > numberOfCoworkers) {
        // reset lunch group because this.coworkers includes me
        lunchGroup = [];
        this.coworkers.forEach(person => {
          lunchGroup.push(person.id);
        });
      } else {
        /**
         * The groupTotal array is an array the length of the # of coworkers
         * At first it contains all the lunch trips the user has had with everyone (the row in relativeLunchFrequencyTable)
         * As we add users to the group their rows get added to the groupTotal so as we search for the next person we find the person which least frequently went to lunch with the remainder of the group
         * This is a group focused lunch group which prioritizes the diversity of the group with people who've least frequently went to lunch with each other
         */
        // initialize lunch group totals
        let groupTotal = [];
        // Fill lunch group with my users row
        for (let i = 0; i < numberOfCoworkers; i++) {
          if (i !== userId) {
            if (this.relativeLunchFrequencyTable[userId][i] !== undefined) {
              groupTotal[i] = this.relativeLunchFrequencyTable[userId][i];
            } else {
              groupTotal[i] = this.relativeLunchFrequencyTable[i][userId];
            }
          } else {
            groupTotal[i] = null;
          }
        }

        while (lunchGroup.length < groupSize) {
          // Find the next best person to join the group (which is the index with the lowest score)
          lunchMateIndex = groupTotal.indexOf(
            // Find the coworker with lowest cumulative lunches with everyone (i.e min in groupTotal)
            Math.min(
              // filter out the nulls because Math.min considers them 0's
              ...groupTotal.filter(function (val) {
                return val !== null;
              })
            )
          );

          // Add the person found to our lunch group
          lunchGroup.push(lunchMateIndex);
          // Update the groupTotal array with the people in the lunch group
          for (let i = 0; i < numberOfCoworkers; i++) {
            if (i !== lunchMateIndex) {
              if (groupTotal[i] !== null) {
                if (
                  this.relativeLunchFrequencyTable[lunchMateIndex][i] !==
                  undefined
                ) {
                  groupTotal[i] +=
                    lunchGroup.length * // weighted by the order of choice/grouping (this could be a unique number per user so a manager could be worth more or if we wanted to group the full-stack team with the design team more often we can )
                    this.relativeLunchFrequencyTable[lunchMateIndex][i];
                } else {
                  groupTotal[i] +=
                    lunchGroup.length *
                    this.relativeLunchFrequencyTable[i][lunchMateIndex];
                }
              }
            } else {
              groupTotal[i] = null;
            }
          }
        }
      }

      // Print out message to lunch group
      let lunchMateMessage =
        "Hey " +
        personName +
        ", why don't you grab lunch with this awesome group of people: <br>";

      for (let i = 0; i < lunchGroup.length; i++) {
        if (lunchGroup[i] !== userId) {
          lunchMateMessage += this.coworkers[lunchGroup[i]]["name"] + "<br>";
        }
      }

      document.getElementById("lunchMateMessage").innerHTML = lunchMateMessage;

      // Tally every coworker that every other coworker went to lunch with
      while (lunchGroup.length > 0) {
        for (let i = 1; i < lunchGroup.length; i++) {
          if (
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] !==
            undefined
          ) {
            this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
              lunchGroup[i],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[i]] + 1
            );
          } else {
            this.relativeLunchFrequencyTable[lunchGroup[i]].splice(
              lunchGroup[0],
              1,
              this.relativeLunchFrequencyTable[lunchGroup[i]][lunchGroup[0]] + 1
            );
          }
        }
        // Increment the number of lunch meets you've been on by 1
        let numberOfLunchMeetsUserHasBeenOn = 1; // intitallty the users been on 0
        // If theyve been on lunch meets before then their index in the relativeLunchFrequencyTable should contain the number of times theyve been on a lunch meet
        if (
          this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] !==
          undefined
        ) {
          numberOfLunchMeetsUserHasBeenOn =
            this.relativeLunchFrequencyTable[lunchGroup[0]][lunchGroup[0]] + 1;
        }

        this.relativeLunchFrequencyTable[lunchGroup[0]].splice(
          lunchGroup[0],
          1,
          numberOfLunchMeetsUserHasBeenOn // increment the number of lunch meets by 1
        );
        // Remove the user we just tallied from the lunchGroup array
        lunchGroup.shift();
      }
      document.getElementById("lunchFrequencyMessage").innerHTML =
        "P.S. Did you know you've been out to lunch with coworkers " +
        this.relativeLunchFrequencyTable[userId][userId] +
        (this.relativeLunchFrequencyTable[userId][userId] > 1 ?
          " times! (including this)" :
          " time! (including this)");
    }
  },
  watch: {
    coworkers: {
      handler() {
        console.log("coworkers changed!");
        if (storageAvailable("localStorage")) {
          localStorage.setItem("coworkers", JSON.stringify(this.coworkers));
        } else if (storageAvailable("sessionStorage")) {
          sessionStorage.setItem("coworkers", JSON.stringify(this.coworkers));
        } else {
          alert(
            "Browser does not support localStorage or sessionStorage; please try using another browser"
          );
        }
      }
    },
    relativeLunchFrequencyTable: {
      handler() {
        console.log("relativeLunchFrequencyTable changed!");
        if (storageAvailable("localStorage")) {
          localStorage.setItem(
            "relativeLunchFrequencyTable",
            JSON.stringify(this.relativeLunchFrequencyTable)
          );
        } else if (storageAvailable("sessionStorage")) {
          sessionStorage.setItem(
            "relativeLunchFrequencyTable",
            JSON.stringify(this.relativeLunchFrequencyTable)
          );
        } else {
          alert(
            "Browser does not support localStorage or sessionStorage; please try using another browser"
          );
        }
      }
    }
  },
  mounted() {
    console.log("App mounted!");
    if (storageAvailable("localStorage")) {
      if (localStorage.getItem("coworkers")) {
        this.coworkers = JSON.parse(localStorage.getItem("coworkers"));
      }
      if (localStorage.getItem("relativeLunchFrequencyTable")) {
        this.relativeLunchFrequencyTable = JSON.parse(
          localStorage.getItem("relativeLunchFrequencyTable")
        );
      }
    } else if (storageAvailable("sessionStorage")) {
      if (sessionStorage.getItem("coworkers")) {
        this.coworkers = JSON.parse(sessionStorage.getItem("coworkers"));
      }
      if (sessionStorage.getItem("relativeLunchFrequencyTable")) {
        this.relativeLunchFrequencyTable = JSON.parse(
          sessionStorage.getItem("relativeLunchFrequencyTable")
        );
      }
    } else {
      alert(
        "Browser does not support localStorage or sessionStorage; please try using another browser"
      );
    }
  }
});

/** Function to check storage type availability from the MDN web docs.
 * For example Safari in some cases did not give access to localStorage
 * For more info go to:
 * https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API */
function storageAvailable(type) {
  try {
    var storage = window[type],
      x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage.length !== 0
    );
  }
}